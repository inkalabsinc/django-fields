from django.db import models
from django_fields.fields import EncryptedCharField


class EncObject(models.Model):
    max_password = 40
    password = EncryptedCharField(max_length=max_password)
